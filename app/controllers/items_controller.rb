class ItemsController < ApplicationController
  def create
  	@dish = Dish.find(params[:dish_id])
  	@cart = Cart.find_by(restaurant_id: @dish.restaurant.id, user_id: current_user.id)
  	if Item.find_by(title: @dish.title, price: @dish.price,  restaurant: @dish.restaurant.title, cart_id: @cart.id)
  		redirect_to restaurants_show_path(@dish.restaurant.id)
  	else
  		Item.create(title: @dish.title, price: @dish.price, qty: 1, restaurant: @dish.restaurant.title, cart_id: @cart.id)
  		redirect_to restaurants_show_path(@dish.restaurant.id)
  	end
  end

  def update
  	@item = Item.find(params[:item_id])
  	if params[:operation] == "plus"
      @item.update_attributes(qty: @item.qty += 1)
    elsif params[:operation] == "minus"
      if @item.qty > 1
       @item.update_attributes(qty: @item.qty -= 1)
      else
      end
   end
   redirect_to restaurants_show_path(@item.cart.restaurant.id)
 end

 def destroy
   @item = Item.find(params[:item_id])
   @cart = Cart.find_by(params[:cart_id])
   @item.destroy
   redirect_to restaurants_show_path(@cart.restaurant.id)
 end
end
