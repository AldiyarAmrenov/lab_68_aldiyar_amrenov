class OrdersController < ApplicationController
  def create
  	@cart = Cart.find_by(params[:cart_id])
  	goods = ""
  	@cart.items.each do |item|
  		goods += "|#{item.title} X #{item.qty}|"
  	end
  	Order.create(client_id: current_user.id, ordered: goods)
    Cart.find_by(params[:cart_id]).destroy
  	redirect_to root_path
  end

  def index
  	@orders = Order.all.where(client_id: current_user.id)
  end

  def show
  end
end
