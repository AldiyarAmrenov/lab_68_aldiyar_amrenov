class RestaurantsController < ApplicationController
  def index
  	@restaurants = Restaurant.all
  end

  def show
  	@restaurant = Restaurant.find(params[:id])
  	if Cart.find_by(user_id: current_user.id, restaurant_id: @restaurant.id)
  		@cart = Cart.find_by(user_id: current_user.id, restaurant_id: @restaurant.id)
  	else
		  @cart = Cart.create(user_id: current_user.id, restaurant_id: @restaurant.id)
	  end
  end
end
