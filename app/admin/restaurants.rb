ActiveAdmin.register Restaurant do
	# See permitted parameters documentation:
	# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
	#
	permit_params :restaurant_id, :title, :desc, :image
	#
	# or
	#
	# permit_params do
	#   permitted = [:permitted, :attributes]
	#   permitted << :other if params[:action] == 'create' && current_user.admin?
	#   permitted
	# end

	form do |f|
		f.inputs do
			f.input :title
			f.input :desc
			if f.object.image.attached?
				f.input :image,
				:as => :file,
				:hint => image_tag(
					url_for(
						f.object.image.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
						)
					)
			else
				f.input :image, :as => :file
			end
		end
		f.actions
	end

	index do
		id_column
		column :image do |restaurant|
			if restaurant.image.attached?
				image_tag restaurant.image.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
			end
		end
		column :title do |restaurant|
			link_to restaurant.title, admin_restaurant_path(restaurant)
		end
		column :desc
		actions
	end

	show do
		attributes_table do
			row :image do |restaurant|
				if restaurant.image.attached?
					image_tag restaurant.image.variant(combine_options: { gravity: 'Center', crop: '50x50+0' })
				end
			end
			row :title
			row :desc
		end
		active_admin_comments
	end

end
