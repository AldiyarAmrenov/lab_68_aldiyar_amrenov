ActiveAdmin.register Dish do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :dish_id, :title, :desc, :price, :image, :restaurant_id
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
form do |f|
	f.inputs do
		f.input :restaurant
		f.input :title
		f.input :desc
		f.input :price
		if f.object.image.attached?
			f.input :image,
			:as => :file,
			:hint => image_tag(
				url_for(
					f.object.image.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
					)
				)
		else
			f.input :image, :as => :file
		end
	end
	f.actions
end

index do
	selectable_column
	id_column
	column :image do |dish|
		if dish.image.attached?
			image_tag dish.image.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
		end
	end
	column :title do |dish|
		link_to dish.title, admin_dish_path(dish)
	end
	column :desc
	column :price
	column :restaurant
	actions
end

show do
	attributes_table do
		row :image do |dish|
			if dish.image.attached?
				image_tag dish.image.variant(combine_options: { gravity: 'Center', crop: '50x50+0' })
			end
		end
		row :title
		row :restaurant
		row :price
		row :desc
	end
	active_admin_comments
end


end
