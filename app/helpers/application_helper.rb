module ApplicationHelper
	def total_price(array)
		sum = 0
		array.each do |item|
			sum += (item.qty * item.price)
		end
		return sum
	end
end
