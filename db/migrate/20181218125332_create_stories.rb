class CreateStories < ActiveRecord::Migration[5.2]
  def change
    create_table :stories do |t|
      t.string :client_first_name
      t.string :client_surname
      t.text :ordered
      t.float :total_price

      t.timestamps
    end
  end
end
