class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.string :title
      t.float :price
      t.integer :qty
      t.string :restaurant

      t.timestamps
    end
  end
end
