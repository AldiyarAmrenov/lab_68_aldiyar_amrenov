# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?

def bellagio_dish_copy_image_fixture(dish, file)
  fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures', 'bellagio', 'dishes', "#{file}.jpg")
  dish.image.attach(io: File.open(fixtures_path), filename: "#{file}.jpg")
end

def raketa_dish_copy_image_fixture(dish, file)
  fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures', 'raketa', 'dishes', "#{file}.jpg")
  dish.image.attach(io: File.open(fixtures_path), filename: "#{file}.jpg")
end

def fort_vernyi_dish_copy_image_fixture(dish, file)
  fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures', 'fort_vernyi', 'dishes', "#{file}.jpg")
  dish.image.attach(io: File.open(fixtures_path), filename: "#{file}.jpg")
end

def bellagio_copy_image_fixture(restaurant, file)
  fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures', 'bellagio', "#{file}.jpg")
  restaurant.image.attach(io: File.open(fixtures_path), filename: "#{file}.jpg")
end

def raketa_copy_image_fixture(restaurant, file)
  fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures', 'raketa', "#{file}.jpg")
  restaurant.image.attach(io: File.open(fixtures_path), filename: "#{file}.jpg")
end

def fort_vernyi_copy_image_fixture(restaurant, file)
  fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures', 'fort_vernyi', "#{file}.jpg")
  restaurant.image.attach(io: File.open(fixtures_path), filename: "#{file}.jpg")
end

restaurants = {
  bellagio: Restaurant.create(title: "Bellagio", desc: "Мировая кухня"),
  raketa: Restaurant.create(title: 'Raketa', desc: "Beer pub"),
  fort_vernyi: Restaurant.create(title: "Форт Верный", desc: "Русская и европейская кухня")
}

bellagio_copy_image_fixture(restaurants[:bellagio], :bellagio)
raketa_copy_image_fixture(restaurants[:raketa], :raketa)
fort_vernyi_copy_image_fixture(restaurants[:fort_vernyi], :fort_vernyi)

bellagio_dish_1 = Dish.create(title: 'Омар', price: 30000.0, desc: "Вареный свежий омар", restaurant: restaurants[:bellagio])
bellagio_dish_copy_image_fixture(bellagio_dish_1, :omar)
bellagio_dish_2 = Dish.create(title: 'Стейк', price: 7000.0, desc: "Мраморная говядина", restaurant: restaurants[:bellagio])
bellagio_dish_copy_image_fixture(bellagio_dish_2, :stick)
bellagio_dish_3 = Dish.create(title: 'Тигровые креветки', price: 35000.0, desc: "Свежие тигровые креветки", restaurant: restaurants[:bellagio])
bellagio_dish_copy_image_fixture(bellagio_dish_3, :tiger_shrimps)

raketa_dish_1 = Dish.create(title: 'GUINNESS', price: 1300.0, desc: "Irish beer", restaurant: restaurants[:raketa])
raketa_dish_copy_image_fixture(raketa_dish_1, :guinness)
raketa_dish_2 = Dish.create(title: 'Murphys', price: 1500.0, desc: "Irish beer", restaurant: restaurants[:raketa])
raketa_dish_copy_image_fixture(raketa_dish_2, :murphys)
raketa_dish_3 = Dish.create(title: 'Samuel Adams', price: 1000.0, desc: "Irish beer", restaurant: restaurants[:raketa])
raketa_dish_copy_image_fixture(raketa_dish_3, :samadams)

fort_vernyi_dish_1 = Dish.create(title: 'Борщ', price: 900.0, desc: "Традиционный борщ", restaurant: restaurants[:fort_vernyi])
fort_vernyi_dish_copy_image_fixture(fort_vernyi_dish_1, :борщ)
fort_vernyi_dish_2 = Dish.create(title: 'Вареники с мясом', price: 1300.0, desc: "Вареники с говядиной", restaurant: restaurants[:fort_vernyi])
fort_vernyi_dish_copy_image_fixture(fort_vernyi_dish_2, :вареники_с_мясом)
fort_vernyi_dish_3 = Dish.create(title: 'Гусь', price: 30000.0, desc: "Свежий запеченый гусь", restaurant: restaurants[:fort_vernyi])
fort_vernyi_dish_copy_image_fixture(fort_vernyi_dish_3, :гусь)