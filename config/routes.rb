Rails.application.routes.draw do

  
  post 'orders/:cart_id/create/' => 'orders#create', as: 'orders_create'
  get 'orders/show'
  get 'orders/index'

  post 'items/:dish_id/create' => 'items#create', as: 'items_create'
  put 'items/:item_id/update/:operation' => 'items#update', as: 'items_update'
  delete 'items/:item_id/destroy/:cart_id' => 'items#destroy', as: 'items_destroy'

  devise_for :users
  root 'restaurants#index'

  get 'restaurants/:id/show' => 'restaurants#show', as: 'restaurants_show'
  get 'dishes/:id/show' => 'dishes#show', as: 'dishes_show'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
